<?php


namespace Smit\Connexys;


class AuthHeader
{
    public $sessionId;

    public function __construct($result)
    {
        $this->sessionId = $result->result->sessionId;
    }
}