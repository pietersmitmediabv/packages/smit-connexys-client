<?php

namespace Smit\Connexys;

use Smit\Connexys\Models\Position;
use SoapClient;
use SoapHeader;

class Client
{
    protected $username;
    protected $password;
    protected $token;
    protected $datacenter;

    public function __construct(array $config)
    {
        // @todo ensure config keys exist or throw exception

        $this->username = $config['username'];
        $this->password = $config['password'];
        $this->token = $config['token'];
        $this->datacenter = $config['datacenter'];
    }

    public function getPositions()
    {
        return array_map(function ($position) {
            return new Position($position);
        }, $this->fetchPositions());
    }

    public function getPositionIds()
    {
        $response = $this->positionsWebserviceCall('getPositionIds');

        return explode(',', $response->result->ids);
    }

    protected function fetchPositions()
    {
        $response = $this->positionsWebserviceCall('getPositions');

        return json_decode(json_encode($response->result->positions), true);
    }

    protected function positionsWebserviceCall($function)
    {
        $client = new SoapClient(__DIR__ . '/Wsdl/cxsPositionWebservice.wsdl', [
            'trace' => 1,
            'cache_wsdl' => WSDL_CACHE_NONE,
        ]);

        return $client->__soapCall($function, [], [], $this->getSoapHeader());
    }

    protected function getSoapHeader()
    {
        $client = new SoapClient(__DIR__ . '/Wsdl/cxsLogin.wsdl', [
            'trace' => 1,
            'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
            'cache_wsdl' => WSDL_CACHE_NONE,
        ]);

        $loginResult = $client->login([
            'username' => $this->username,
            'password' => $this->password . $this->token
        ]);

        return new SoapHeader(
            "https://{$this->datacenter}.salesforce.com/services/Soap/class/cxsrec/cxsPositionWebservice",
            'SessionHeader',
            new AuthHeader($loginResult),
            false
        );
    }
}

