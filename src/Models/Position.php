<?php

namespace Smit\Connexys\Models;

class Position
{
    protected $data;
    protected $internalId;

    public function __construct(array $data, int $internalId = null)
    {
        $this->data = $data;
        $this->internalId = $internalId;
    }

    public function getId()
    {
        return $this->data['id'];
    }

    public function getInternalId()
    {
        return $this->internalId;
    }

    public function getTitle()
    {
        return $this->data['name'];
    }

    public function getHours()
    {
        return $this->getCriterium('Aantal uren');
    }

    public function getSector()
    {
        return $this->getCriterium('Functiegroep');
    }

    public function getEducationLevel()
    {
        return $this->getCriterium('Opleidingsniveau');
    }

    public function getLocation()
    {
        return $this->getCriterium('Werkgebied');
    }

    public function getCompanyDescription()
    {
        return $this->data['companyInformation'];
    }

    public function getJobDescription()
    {
        return $this->data['jobDescription'];
    }

    public function getStartDate()
    {
        return $this->data['startDate'];
    }

    public function getRecruiter()
    {
        return $this->data['recruiter']['name'];
    }

    public function getPublicData()
    {
        return [
            'id' => $this->getId(),
            'internal_id' => $this->getInternalId(),
            'title' => $this->getTitle(),
            'hours' => $this->getHours(),
            'sector' => $this->getSector(),
            'education_level' => $this->getEducationLevel(),
            'location' => $this->getLocation(),
            'company_description' => $this->getCompanyDescription(),
            'job_description' => $this->getJobDescription(),
            'job_description_stripped' => strip_tags($this->getJobDescription()),
            'start_date' => $this->getStartDate(),
            'recruiter' => $this->getRecruiter(),
        ];
    }

    public function getData()
    {
        return $this->data;
    }

    protected function getCriterium($key)
    {
        $filtered = array_filter($this->data['criteria'], function ($criterium) use ($key) {
            return $criterium['key'] === $key;
        });
        sort($filtered);

        if (!count($filtered)) {
            return null;
        }

        return $filtered[0]['value'];
    }
}
